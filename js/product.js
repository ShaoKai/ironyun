$(function(){
	var spinner = $( ".vmSpinner" ).spinner({
		spin: function(event, ui){
			if( ui.value < 0){
				$(this).spinner("value", 0);
				return false;
			}
		}
	});
});
